package find4sport.com.appficherosacdat;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.CountDownTimer;
import android.os.Environment;
import android.preference.PreferenceActivity;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {

    private EditText etImagenes;
    private EditText etFrases;
    private TextView tvFrases;
    private ImageView ivImagen;
    private Button btnDescargar;
    private ConstraintLayout clActivity;
    private Memoria memoria;
    private ProgressDialog progress;

    private long tiempo = 6000L;
    private String frases;
    private String imagenes;
    private boolean resultadoFrases = false;
    private boolean resultadoImagenes = false;

    private CountDownTimer countDownTimer;

    private static final String FRASES = "frases.txt";
    private static final String IMAGENES = "imagenes.txt";
    private static final String ERROR = "errores.txt";

    private static final String SERVIDOR = "http://alumno.mobi/~alumno/superior/aranda/upload.php";
    private static final String SERVIDOR_ERROR = "http://alumno.mobi/~alumno/superior/aranda/errores.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        memoria = new Memoria(this);
        progress = new ProgressDialog(this);

        clActivity = findViewById(R.id.clActivity);
        etFrases = findViewById(R.id.etFrases);
        etImagenes = findViewById(R.id.etImagenes);
        ivImagen = findViewById(R.id.imageView);
        tvFrases = findViewById(R.id.tvFrases);
        btnDescargar = (Button)findViewById(R.id.btnDescargar);
        btnDescargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!resultadoFrases || !resultadoImagenes) {
                    descargar(etFrases.getText().toString(), FRASES);
                    descargar(etImagenes.getText().toString(), IMAGENES);
                    iniciarPresentacion();
                } else {
                    Snackbar.make(clActivity, "Error en la descarga", Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    private void descargar(String ruta, final String rutaLocal) {
        if (!ruta.startsWith("http://") && !ruta.startsWith("https://"))
            ruta = "http://" + ruta;
        RestClient.get(ruta, new FileAsyncHttpResponseHandler(this) {
            @Override
            public void onStart() {
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setMessage("Conectando . . .");
                progress.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        RestClient.cancelRequests(getApplicationContext(), true);
                    }
                });
                progress.show();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progress.dismiss();
                errores("Error en la conexión");
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, File response) {
                progress.dismiss();
                try {
                    BufferedReader reader = new BufferedReader(new FileReader(file));
                    StringBuilder content = new StringBuilder();
                    String line;
                    while((line = reader.readLine()) != null)
                        content.append(line).append("\n");
                    reader.close();
                    if (memoria.disponibleEscritura()) {
                        memoria.escribirExterna(rutaLocal, content.toString(), false, "UTF-8");
                        Snackbar.make(clActivity, "Archivo guardado con exito", Snackbar.LENGTH_LONG).show();
                        if (memoria.disponibleLectura()) {
                            Resultado resultado = memoria.leerExterna(rutaLocal, "UTF-8");
                            if (rutaLocal == FRASES){
                                if(resultado.getResultado()) {
                                    frases = resultado.getContenido();
                                    resultadoFrases = resultado.getResultado();
                                }
                            }else if(rutaLocal == IMAGENES){
                                if(resultado.getResultado()) {
                                    imagenes = resultado.getContenido();
                                    resultadoImagenes = resultado.getResultado();
                                }
                            }
                        }
                        iniciarPresentacion();
                    } else {
                        errores("No se leyó el archivo");
                    }
                } catch (IOException e) {
                    errores("Fallo de lectura del archivo");
                } catch (Exception e) {
                    errores("Error: " + e.getMessage());
                }
            }
        });
    }

    private void iniciarPresentacion() {
        if(resultadoFrases && resultadoImagenes)
            countDownTimer = new CountDownTimer(999999, tiempo) {
                String[] arrayFrases = frases.split("\n");
                String[] arrayImagenes = imagenes.split("\n");
                int contador = 0;

                public void onTick(long millisUntilFinished) {
                    contador++;
                    cambiar(
                            arrayFrases[contador % arrayFrases.length],
                            arrayImagenes[contador % arrayImagenes.length]
                    );
                }
                public void onFinish() {
                    Snackbar.make(clActivity, "Fin", Snackbar.LENGTH_LONG).show();
                }
            }.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        countDownTimer.cancel();
    }

    private void cambiar(String frase, final String ruta) {
        tvFrases.setText(frase);
        RestClient.get(ruta, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Picasso.with(MainActivity.this)
                        .load(ruta)
                        .error(R.drawable.icon_error)
                        .placeholder(R.drawable.icon_placeholder)
                        .into(ivImagen);
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                errores("No se puede descargar de: " + ruta);
            }
        });
    }


    private void errores(final String errorMsg) {
        final StringBuilder content = new StringBuilder();
        final Resultado[] resultado = { new Resultado() };

        RestClient.get(SERVIDOR_ERROR, new FileAsyncHttpResponseHandler(this) {
            @Override
            public void onStart() {

            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                Snackbar.make(clActivity, "Fallo al intentar cambiar la imagen...", Snackbar.LENGTH_LONG).show();
                ivImagen.setImageDrawable(getResources().getDrawable(R.drawable.icon_error));
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, File response) {
                try {
                    BufferedReader reader = new BufferedReader(new FileReader(file));
                    String line;
                    while((line = reader.readLine()) != null)
                        content.append(line).append("\n");
                    reader.close();
                    content.append("[").append(new Date()).append("]: ").append(errorMsg).append("\n");
                    if (memoria.disponibleEscritura()) {
                        memoria.escribirExterna(ERROR, content.toString(), true, "UTF-8");
                        if (memoria.disponibleLectura())
                            resultado[0] = memoria.leerExterna(ERROR, "UTF-8");
                        else
                            Snackbar.make(clActivity, "Imposible leer el archivo...", Snackbar.LENGTH_LONG).show();
                    }
                    subirErrores(resultado);
                } catch (IOException e) {
                    Snackbar.make(clActivity, "Fallo al leer el archivo", Snackbar.LENGTH_LONG).show();
                } catch (Exception e) {
                    Snackbar.make(clActivity, "Error: " + e.getMessage(), Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    private void subirErrores(Resultado[] resultado) {
        if(resultado[0].getResultado()) {
            RequestParams params = new RequestParams();
            try {
                params.put("fileToUpload", new File(Environment.getExternalStorageDirectory(), ERROR));
                RestClient.post(SERVIDOR, params, new TextHttpResponseHandler() {
                    @Override
                    public void onStart() {
                        Snackbar.make(clActivity, "Subiendo archivo de errores", Snackbar.LENGTH_LONG).show();
                    }
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String response) {
                        Snackbar.make(clActivity, "Archivo de errores actualizado", Snackbar.LENGTH_LONG).show();
                    }
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String response, Throwable t) {
                        Snackbar.make(clActivity, "Error al actualizar el archivo de errores", Snackbar.LENGTH_LONG).show();
                    }
                });
            } catch (FileNotFoundException e) {
                Snackbar.make(clActivity, "Error: " + e.getMessage(), Snackbar.LENGTH_LONG).show();
            }
        } else {
            Snackbar.make(clActivity, "Error al leer el archivo de errores", Snackbar.LENGTH_LONG).show();
        }
    }
}
