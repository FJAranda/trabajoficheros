package find4sport.com.appficherosacdat;

/**
 * Created by Javi on 10/12/17.
 */

public class Resultado {
    private boolean resultado;
    private String mensaje;
    private String contenido;

    public boolean getResultado() {
        return resultado;
    }
    public void setResultado(boolean resultado) {
        this.resultado = resultado;
    }
    public String getMensaje() {
        return mensaje;
    }
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
    public String getContenido() {
        return contenido;
    }
    public void setContenido(String contenido) {
        this.contenido = contenido;
    }
}
