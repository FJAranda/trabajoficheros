package find4sport.com.appficherosacdat;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;

/**
 * Created by Javi on 10/12/17.
 */

public class Memoria {
    private static final int FILE_PICKER_REQUEST_CODE = 1;
    public static final String UTF8 = "UTF-8";
    private Context context;

    public Memoria(Context context) {
        this.context = context;
    }

    private boolean escribir(FileOutputStream stream, String cadena, String codigo) {
        BufferedWriter out = null;
        boolean correcto = false;
        try {
            out = new BufferedWriter(new OutputStreamWriter(stream, codigo));
            out.write(cadena);
        } catch (IOException e) {
            Log.e("Error de E/S", e.getMessage());
        } finally {
            try {
                if (out != null) {
                    out.flush();
                    out.close();
                    correcto = true;
                }
            } catch (IOException e) {
                Log.e("Error al cerrar", e.getMessage());
            }
        }
        return correcto;
    }

    public boolean disponibleEscritura() {

        boolean escritura = false;

        String estado = Environment.getExternalStorageState();

        if (estado.equals(Environment.MEDIA_MOUNTED))
            escritura = true;
        return escritura;
    }

    public boolean disponibleLectura() {
        boolean lectura = false;

        String estado = Environment.getExternalStorageState();
        if (estado.equals(Environment.MEDIA_MOUNTED_READ_ONLY) || estado.equals(Environment.MEDIA_MOUNTED))
            lectura = true;
        return lectura;
    }

    public boolean escribirExterna(String fichero, String cadena, Boolean anadir, String codigo) {
        File miFichero, tarjeta;
        tarjeta = Environment.getExternalStorageDirectory();
        miFichero = new File(tarjeta.getAbsolutePath(), fichero);
        FileOutputStream stream = null;
        boolean resultado = false;
        try {
            resultado = escribir(new FileOutputStream(miFichero, anadir), cadena, codigo);
        } catch (FileNotFoundException e) {
            Log.e("Archivo inexistente: ", e.getMessage());
        } finally {
            if (stream != null)
                try {
                    stream.close();
                } catch (IOException e) {
                    Log.e("Error al cerrar", e.getMessage());
                }
        }
        return resultado;
    }

    private Resultado leer(FileInputStream stream, String codigo) {
        BufferedReader reader = null;
        StringBuilder miCadena = new StringBuilder();
        Resultado resultado = new Resultado();
        resultado.setResultado(true);
        try {
            reader = new BufferedReader(new InputStreamReader(stream, codigo));
            int n;
            while ((n = reader.read()) != -1)
                miCadena.append((char) n);
        } catch (IOException e) {
            Log.e("Error", e.getMessage());
            resultado.setResultado(false);
            resultado.setMensaje(e.getMessage());
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                    resultado.setContenido(miCadena.toString());
                }
            } catch (IOException e) {
                Log.e("Error al cerrar", e.getMessage());
                resultado.setResultado(false);
                resultado.setMensaje(e.getMessage());
            }
        }
        return resultado;
    }

    public Resultado leerExterna(String fichero, String codigo) {
        File miFichero, tarjeta;
        tarjeta = Environment.getExternalStorageDirectory();
        miFichero = new File(tarjeta.getAbsolutePath(), fichero);
        FileInputStream stream = null;
        Resultado resultado = new Resultado();
        resultado.setResultado(false);
        try {
            resultado = leer(new FileInputStream(miFichero), codigo);
        } catch (FileNotFoundException e) {
            Log.e("Archivo inexistente: ", e.getMessage());
        } finally {
            if (stream != null)
                try {
                    stream.close();
                } catch (IOException e) {
                    Log.e("Error al cerrar", e.getMessage());
                }
        }
        return resultado;
    }
}